Drupal MuseScore module:
------------------------
Maintainers:
  Andrew Dresden (http://drupal.org/user/1865114)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
The MuseScore module can display up to 10 MuseScore titles in a block.
Clicking the title opens and plays the score.

To configure, simply enter the MuseScore URL and title for each score on
the block settings page, then assign the MuseScore block to a region of
your site. Visit http://www.musescore.com to search for scores.

Note:  At present, the Flash browser plug-in is required to play back
the scores.


Installation:
------------
1. Download and unpack the MuseScore module directory in your modules
folder (this will usually be "sites/all/modules/").

2. Go to "Administer" -> "Modules" and enable the module.


Configuration:
-------------
1. Go to "Structure" -> "Blocks" -> "MuseScore".

2. Select the number of MuseScores to display in the block from the 
    "MuseScores to Display" select menu and click "Save Block"

3. Click the "MuseScores" tab. Enter the title and URL for each 
   score. The URL must be a musescore URL; either 
   http://www.musescore.com/[user name]/[score name]
   or http://musescore.com/user/[user ID]/scores/[score ID]

4. Assign the "MuseScore" block to a region of your site.


TODO:
------------------------------------
Add option/block to display all scores for a particular user.
